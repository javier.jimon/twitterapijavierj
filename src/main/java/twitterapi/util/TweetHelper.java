package twitterapi.util;


import twitterapi.dto.TweetDto;
import twitterapi.dto.TweetRequest;
import twitterapi.entity.Tweet;


public class TweetHelper {

	public static Tweet parseTweet(TweetRequest request) {
		Tweet Tweet = new Tweet(request.getUser(), request.getText(), request.getLocalization(), request.isVerified());
		return Tweet;
	}

	public static TweetDto convert(Tweet tweet) {
		TweetDto dto = new TweetDto();
		dto.setTweet(tweet);
		return dto;
	}

	
}
