package twitterapi.util;


public class TweetHashtagCount implements Comparable<TweetHashtagCount>{
	private String hashtag;
	private int count;
	
	public TweetHashtagCount(String hashtag, int count) {
		super();
		this.hashtag = hashtag;
		this.count = count;
	}
	
	public String getHashtag() {
		return hashtag;
	}
	
	public void setHashtag(String hashtag) {
		this.hashtag = hashtag;
	}
	
	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public int compareTo(TweetHashtagCount o) {
		return (int) o.getCount() - this.count;
	}
}
