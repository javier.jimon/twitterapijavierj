package twitterapi.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Twitter4jPropertiesValues {
	InputStream inputStream;
	private final String OAUTH_API_KEY = "oauth.consumerKey";
	private final String OAUTH_API_KEY_SECRET = "oauth.consumerSecret";
	private final String OAUTH_ACCESS_TOKEN = "oauth.accessToken";
	private final String OAUTH_ACCESS_TOKEN_SECRET = "oauth.accessTokenSecret";
	private final String propertiesFilePath = "./resources/twitter4j.properties";
	
	public String getPropValue(String property) throws IOException { 
		try {
			Properties prop = new Properties();
			String propFileName = propertiesFilePath;
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
 
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
 
			// get the property value
			switch(property) {
				case "OAUTH_API_KEY":
					return prop.getProperty(OAUTH_API_KEY);
				case "OAUTH_API_KEY_SECRET":
					return prop.getProperty(OAUTH_API_KEY_SECRET);
				case "OAUTH_ACCESS_TOKEN":
					return prop.getProperty(OAUTH_ACCESS_TOKEN);
				case "OAUTH_ACCESS_TOKEN_SECRET":
					return prop.getProperty(OAUTH_ACCESS_TOKEN_SECRET);
				default: 
					return "";
			}
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			inputStream.close();
		}
		return "";
	}
}
