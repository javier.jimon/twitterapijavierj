package twitterapi.dto;

public class TweetRequest {

	 private String user;
	 private String text;
	 private String localization;
	 private boolean verified;
	 
	public TweetRequest(String user, String text, String localization, boolean verified) {
		super();
		this.user = user;
		this.text = text;
		this.localization = localization;
		this.verified = verified;
	}
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getLocalization() {
		return localization;
	}
	public void setLocalization(String localization) {
		this.localization = localization;
	}
	public boolean isVerified() {
		return verified;
	}
	public void setVerified(boolean verified) {
		this.verified = verified;
	}
  	
}