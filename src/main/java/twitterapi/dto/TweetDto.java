package twitterapi.dto;

import twitterapi.entity.Tweet;

public class TweetDto {
	
	private Tweet tweet;

	public Tweet getTweet() {
		return tweet;
	}

	public void setTweet(Tweet tweet) {
		this.tweet = tweet;
	}
	

}
