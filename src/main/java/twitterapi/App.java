package twitterapi;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterException;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;
import twitterapi.controller.TweetController;
import twitterapi.dto.TweetRequest;
import twitterapi.util.Twitter4jPropertiesValues;

@EnableAutoConfiguration
@ComponentScan(basePackages="twitterapi")
@Configuration
@SpringBootApplication
public class App {
	private static String OAUTH_API_KEY = "OAUTH_API_KEY";
	private static String OAUTH_API_KEY_SECRET = "OAUTH_API_KEY_SECRET";
	private static String OAUTH_ACCESS_TOKEN = "OAUTH_ACCESS_TOKEN";
	private static String OAUTH_ACCESS_TOKEN_SECRET = "OAUTH_ACCESS_TOKEN_SECRET";
	private Twitter4jPropertiesValues twitter4jPropertiesValues;
	
	private static List<String> languages = Arrays.asList("es", "fr", "it");
	private static int minFollowers = 1500;
	
	@Autowired
	private TweetController tweetController;
	
	public static void main(String[] args)throws TwitterException, IOException{	    
        SpringApplication.run(App.class, args);
    }
	
	@PostConstruct
	private void init(){
		 StatusListener listener = new StatusListener(){	    	
		        public void onStatus(Status status) {
		        	if(status.getUser().getFollowersCount() > minFollowers
		        			&& languages.contains(status.getLang().toLowerCase())) {
		        		TweetRequest tweetRequest = new TweetRequest(status.getUser().getName(),
		        				status.getText(), status.getLang(), status.getUser().isVerified());
		        		tweetController.saveTweet(tweetRequest);
		            }
		        }
		        
		        public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {}
		        public void onTrackLimitationNotice(int numberOfLimitedStatuses) {}
		        public void onException(Exception ex) {
		            ex.printStackTrace();
		        }
				@Override
				public void onScrubGeo(long userId, long upToStatusId) {}
				@Override
				public void onStallWarning(StallWarning warning) {}
		    };
		    
		    twitter4jPropertiesValues = new Twitter4jPropertiesValues();
		    
		    ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		    try {
				configurationBuilder.setOAuthConsumerKey(twitter4jPropertiesValues.getPropValue(OAUTH_API_KEY))
				        .setOAuthConsumerSecret(twitter4jPropertiesValues.getPropValue(OAUTH_API_KEY_SECRET))
				        .setOAuthAccessToken(twitter4jPropertiesValues.getPropValue(OAUTH_ACCESS_TOKEN))
				        .setOAuthAccessTokenSecret(twitter4jPropertiesValues.getPropValue(OAUTH_ACCESS_TOKEN_SECRET))
				        .setJSONStoreEnabled(true);
			} catch (IOException e) {
				e.printStackTrace();
			}
		    
		    TwitterStream twitterStream = (new TwitterStreamFactory(configurationBuilder.build())).getInstance();	    
		    twitterStream.addListener(listener);
		    // sample() method internally creates a thread which manipulates TwitterStream and calls these adequate listener methods continuously.
		    twitterStream.sample();
	}
	
}
