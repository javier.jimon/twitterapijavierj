package twitterapi.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import twitterapi.dto.TweetDto;
import twitterapi.dto.TweetRequest;
import twitterapi.entity.Tweet;
import twitterapi.service.ITweetService;
import twitterapi.util.TweetHashtagCount;
import twitterapi.util.TweetHelper;
 

@RestController
public class TweetController {

	@Autowired
    private ITweetService tweetService;
	
	@RequestMapping(value="/saveTweet", method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TweetDto> saveTweet(@RequestBody TweetRequest request) {		
		Tweet tweet = TweetHelper.parseTweet(request);
		ResponseEntity<TweetDto> responseEntity = null;
		TweetDto response = new TweetDto();
		
		Tweet newTweet = tweetService.save(tweet);
		response.setTweet(newTweet);
		responseEntity =  ResponseEntity.status(HttpStatus.CREATED).body(response);	
		
        return responseEntity;
    }
 
	@RequestMapping(value="/getTweets", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TweetDto>> getTweets() {
		List<TweetDto> response = new ArrayList<TweetDto>();
		for(Tweet tw : tweetService.findAll()) {
			TweetDto twDto = new TweetDto();
			twDto.setTweet(tw);
			response.add(twDto);
		}
		
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
	
	@RequestMapping(value="/validateTweet/{id}", method = RequestMethod.PUT, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TweetDto> validateTweet(@PathVariable("id") String id) {
		Optional<Tweet> tweet = tweetService.findOne(Long.parseLong(id));
		if(tweet.isPresent()) {
			tweet.get().setVerified(true);
			Tweet response = tweetService.update(tweet.get());
			
	        return ResponseEntity.status(HttpStatus.ACCEPTED).body(TweetHelper.convert(response));	
		}
		else 
			return new ResponseEntity<TweetDto>(HttpStatus.NOT_FOUND);
    }
	
	@RequestMapping(value="/getTweetsValidatedByUser/{name}", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TweetDto>> getTweetsValidatedByUser(@PathVariable("name") String name) {
		List<TweetDto> response = new ArrayList<TweetDto>();
		for(Tweet tw : tweetService.findAll()) {
			if(tw.getUser().equals(name) && tw.isVerified()) {			
				TweetDto twDto = new TweetDto();
				twDto.setTweet(tw);
				response.add(twDto);
			}
		}
		
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
	
	@RequestMapping(value="/getHashtags", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<String>> getHashtags(@RequestParam(value="count", defaultValue="10") int count) {
		List<String> response = new ArrayList<String>();
		for(Tweet tw : tweetService.findAll()) {
			String[] tweetParts = tw.getText().split("#");
			if(tweetParts.length > 1) {
				for(int i = 1; i < tweetParts.length; i++) {
					String sentence = tweetParts[i];
					String hashtag = sentence.split(" ")[0];
					hashtag = hashtag.split("\n")[0];
					response.add(hashtag);
				}
			}
		}
		
        return ResponseEntity.status(HttpStatus.OK).body(getMostUsed(response, count));
    }

	private List<String> getMostUsed(List<String> list, int limit) {
		List<String> response = new ArrayList<String>();
		
		List<TweetHashtagCount> aux = new ArrayList<TweetHashtagCount>();
		for(String hashtag : list) {
			aux.add(new TweetHashtagCount(hashtag, 1));
		}
		
		Map<String, Integer> map = new HashMap<>();
		for (TweetHashtagCount t : aux) {
			Integer val = map.get(t.getHashtag());
			int newVal = val == null ? 1 : val + 1;
			map.put(t.getHashtag(), newVal);
		}

		aux = new ArrayList<TweetHashtagCount>();
		for(String s : map.keySet()) {
			aux.add(new TweetHashtagCount(s, map.get(s)));
		}
		
		Collections.sort(aux);
		
		for(int i = 0; i < limit && i < aux.size(); i++) {
			response.add(aux.get(i).getHashtag());
		}
		
		return response;
	}
}
