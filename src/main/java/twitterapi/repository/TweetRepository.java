package twitterapi.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import twitterapi.entity.Tweet;
 

@Transactional
@Repository
public interface TweetRepository extends CrudRepository<Tweet, Long>{}
