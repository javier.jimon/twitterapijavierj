package twitterapi.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import twitterapi.entity.Tweet;
import twitterapi.repository.TweetRepository;
import twitterapi.service.ITweetService;


@Service
public class TweetServiceImpl implements ITweetService{

	@Autowired
	private TweetRepository repository;

	@Override
	public Iterable<Tweet> findAll() {
		return repository.findAll();
	}

	@Override
	public Tweet save(Tweet Tweet) {
		return repository.save(Tweet);
	}

	@Override
	public Optional<Tweet> findOne(Long id) {
		return repository.findById(id);
	}

	@Override
	public void delete(Tweet Tweet) {
		repository.delete(Tweet);
	}

	@Override
	public Tweet update(Tweet Tweet) {
		return repository.save(Tweet);
	}
	

}
