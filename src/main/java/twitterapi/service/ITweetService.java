package twitterapi.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import twitterapi.entity.Tweet;
 

@Service
public interface ITweetService {

	Iterable<Tweet> findAll();

	Tweet save(Tweet Tweet);
	
	Optional<Tweet> findOne(Long id);
	
	void delete(Tweet Tweet);
	
	Tweet update(Tweet Tweet);

}
