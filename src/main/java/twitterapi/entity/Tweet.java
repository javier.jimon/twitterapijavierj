package twitterapi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Tweet {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
   
    private String user;
    
    @Column(length=1000)
    private String text;
    
    private String localization;
    
    private boolean verified;
 
    public Tweet() {
    	super();
    }
	
	public Tweet(String user, String text, String localization, boolean verified) {
		super();
		this.user = user;
		this.text = text;
		this.localization = localization;
		this.verified = verified;
	}

	public long getId() {
		return id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getLocalization() {
		return localization;
	}

	public void setLocalization(String localization) {
		this.localization = localization;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}
 
     
}